import flask_jwt_extended
import flask_sqlalchemy

jwt=flask_jwt_extended.JWTManager()
db=flask_sqlalchemy.SQLAlchemy()
