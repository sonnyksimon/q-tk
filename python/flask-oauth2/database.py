import extensions

class TokenBlacklist(extensions.db.Model):
    id = extensions.db.Column(extensions.db.Integer, primary_key=True)
    jti = extensions.db.Column(extensions.db.String(36), nullable=False)
    token_type = extensions.db.Column(extensions.db.String(10), nullable=False)
    user_identity = extensions.db.Column(extensions.db.String(50), nullable=False)
    revoked = extensions.db.Column(extensions.db.Boolean, nullable=False)
    expires = extensions.db.Column(extensions.db.DateTime, nullable=False)

    def to_dict(self):
        return {
            'token_id': self.id,
            'jti': self.jti,
            'token_type': self.token_type,
            'user_identity': self.user_identity,
            'revoked': self.revoked,
            'expires': self.expires
        }
