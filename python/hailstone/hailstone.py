#!/usr/bin/env python
"""hailstone: sequences that go up and down just like a hailstone """
__version__ = "0.0.1"
__author__ = "Sonny Kothapally <me@sonnyksimon.com>"
__copyright__ = "MIT"
__contributors = []

def hailstone(n):
    """ compute the hailstone sequence starting with n and ending with 1 """
    lst = []

    while n != 1:
        lst.append(n)
        if (n % 2) == 0:
            n = n/2
        else:
            n = 3*n + 1

    lst.append(n)
    return lst

def max(l):
    """ returns the largest element in a list l
        Requires l to be nonempty """
    m = 0
    for x in l:
        if x > m:
            m = x
    return m

if __name__ == "__main__":
    for n in range (1,1000000):
        l = hailstone(n)
        print(str(n)+" >>> "+str(max(l))) # print the peak of the hailstone
