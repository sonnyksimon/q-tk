import os
import sys
from setuptools import setup, find_packages, Extension

commands={}

setup(
    name="az",
    version="",
    install_requires=[
        "flask",
        "mako",
        "Jinja2",
        "cython",
        "SQLAlchemy",
        "BeautifulSoup",
        "psycopg2",
        "firebase_admin",
        "py-bcrypt",
        "requests",
        "webtest",
    ],
    tests_require=[],
    test_suite="",
    dependency_links=[],
    packages=find_packages(exclude=["ez_setup"]),
    cmdclass=commands,
    ext_modules="",
    entry_points="""
    [paste.app_factory]
    main=az:make_app
    """,
)
