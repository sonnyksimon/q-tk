#!/usr/bin/env python
# notsotrivial_backpropagation.py
# (https://iamtrask.github.io/2015/07/12/basic-python-network/)

"""

Image recognition is a similar problem to that in `trivial_backpropagation.py`.
If one had 100 identically sized images of pipes and bicycles, no individual 
pixel position would directly correlate with the presence of a bicycle or pipe. 

The pixels might as well be random from a purely statistical point of view. 
However, certain combinations of pixels are not random, namely the combination 
that forms the image of a bicycle or a person.

Strategy:

In order to first combine pixels into something that can then have a one-to-one 
relationship with the output, we need to add another layer. Our first layer 
will combine the inputs, and our second layer will then map them to the output 
using the output of the first layer as input. Before we jump into an implementation 
though, take a look at this table.

| Inputs (l0) | Hidden Weights (l1)   | Output (l2) |
|-------------|-----------------------|-------------|
| 0 | 0 | 1   | 0.1 | 0.2 | 0.5 | 0.2 | 0           |
| 0 | 1 | 1   | 0.2 | 0.6 | 0.7 | 0.1 | 1           |
| 1 | 0 | 1   | 0.3 | 0.2 | 0.3 | 0.9 | 1           |
| 1 | 1 | 1   | 0.2 | 0.1 | 0.3 | 0.8 | 0           |

If we randomly initialize our weights, we will get hidden state values for layer 1. 
Notice anything? The **second column (second hidden node), has a slight correlation 
with the output already!** It's not perfect, but it's there. Believe it or not, 
this is a huge part of how neural networks train. (Arguably, it's the only way 
that neural networks train.) What the training below is going to do is amplify 
that correlation. It's both going to update syn1 to map it to the output, and update 
syn0 to be better at producing it from the input!

"""

import numpy as np

def nonlin(x,deriv=False):
	if(deriv==True):
	    return x*(1-x)

	return 1/(1+np.exp(-x))

if __name__ == "__main__":
    
    X = np.array([[0,0,1],
                [0,1,1],
                [1,0,1],
                [1,1,1]])
                    
    y = np.array([[0],
                [1],
                [1],
                [0]])

    np.random.seed(1)

    # randomly initialize our weights with mean 0
    syn0 = 2*np.random.random((3,4)) - 1
    syn1 = 2*np.random.random((4,1)) - 1

    for j in range(60000):

        # Feed forward through layers 0, 1, and 2
        l0 = X
        l1 = nonlin(np.dot(l0,syn0))
        l2 = nonlin(np.dot(l1,syn1))

        # how much did we miss the target value?
        l2_error = y - l2
        
        if (j% 10000) == 0:
            print("Error:" + str(np.mean(np.abs(l2_error))))
            
        # in what direction is the target value?
        # were we really sure? if so, don't change too much.
        l2_delta = l2_error*nonlin(l2,deriv=True)

        # how much did each l1 value contribute to the l2 error (according to the weights)?
        l1_error = l2_delta.dot(syn1.T)
        
        # in what direction is the target l1?
        # were we really sure? if so, don't change too much.
        l1_delta = l1_error * nonlin(l1,deriv=True)

        syn1 += l1.T.dot(l2_delta)
        syn0 += l0.T.dot(l1_delta)
