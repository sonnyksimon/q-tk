from flask import Blueprint, request, jsonify, abort
from auth import login_required, current_user

products_page = Blueprint('products', __name__, template_folder='templates')

def get_products():
    return jsonify(dict(message='Handling GET requests to /products')), 200 

@login_required
def create_product():
    try:
        data = request.get_json()
        product = dict(name=data['name'], price=data['price'])
        curr_usr = current_user()
        return jsonify(dict(message='Handling POST requests to /products', createdProduct=product, user=curr_usr)), 201
    except:
        raise Exception("Invalid input")

@products_page.route('/api/products', methods=['GET','POST'])
def products():
    if request.method == 'GET':
        return get_products()
    elif request.method == 'POST':
        return create_product()

@products_page.route('/api/products/<productId>', methods=['GET','PATCH','DELETE'])
def product(productId):
    if request.method == 'GET':
        id = productId
        if id == 'special':
            return jsonify(dict(message='You discovered the special ID', id=id)), 200
        else:
            return jsonify(dict(message='You passed an ID')), 200
    elif request.method == 'PATCH':
        return jsonify(dict(message='Updated product!')), 200
    elif request.method == 'DELETE':
        return jsonify(dict(message='Deleted product!')), 200
