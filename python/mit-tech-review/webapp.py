#!/usr/bin/env python
import flask
import firebase_admin
from firebase_admin import credentials, db
import os,sys,json

production_mode = False
firebase_admin.initialize_app(credentials.Certificate('firebase-creds.json'), dict(databaseURL='https://mittechreview-b4dc8.firebaseio.com/'))

root = db.reference('/')
thing = root.child('thing')
version = root.child('version')
datum = root.child('datum')

def get_thing(id):
    for k,v in thing.get().items():
        if v['id']==id:
            return v
    return None

def create_thing(name):
    ref = thing.push()
    ref.set(dict(id=ref.key,name=name))
    return get_thing(ref.key)

def get_datum(thing_id,key):
    for k,v in datum.get().items():
        if v['thing_id']==thing_id and v['key']==key:
            return v
    return None

def create_datum(thing_id,key,val):
    if not get_thing(thing_id): return None
    ref = datum.push()
    ref.set(dict(id=ref.key,thing_id=thing_id,key=key,val=val))
    return get_datum(thing_id,key)

app = flask.Flask(__name__)

@app.route("/")
def hello():
    return "Hello, world!"

@app.route("/api/login", methods=['POST'])
def login():
    data = [
        dict(
            access_token="flask",
            expires_in=3600,
            scope="*",
            token_type="Bearer"
        )
    ]
    return flask.Response(json.dumps(data[0]), mimetype='application/json')

@app.route("/api/signup", methods=['POST'])
def signup():
    data = flask.request.get_json()
    username = data.get('username', None)
    password = data.get('password', None)
    
    r = dict(success=False)

    if username and password:
        _thing = create_thing('user')
        
        if _thing.get('id', False):
            _username = create_datum(_thing['id'], 'username', username)
            _password = create_datum(_thing['id'], 'password', password)
            r['success']=bool(_username and _password)
        
    return flask.Response(json.dumps(r), mimetype='application/json')

@app.errorhandler(404)
def page_not_found(e):
    return flask.render_template("404.html"), 404

if __name__ == "__main__": app.run(debug=not(production_mode))
