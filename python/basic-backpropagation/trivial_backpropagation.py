#!/usr/bin/env python
# trivial_backpropagation.py
# (https://iamtrask.github.io/2015/07/12/basic-python-network/)

"""

A neural network trained with backpropagation is attempting to use input to predict output.

| Inputs    | Output |
|-----------|--------|
| 0 | 0 | 1 | 0      |
| 1 | 1 | 1 | 1      |
| 1 | 0 | 1 | 1      |
| 0 | 1 | 1 | 0      |

Consider trying to predict the output column given the three input columns. 
We could solve this problem by simply measuring `statistics` between the input values and 
the output values. If we did so, we would see that the leftmost input column is perfectly 
correlated with the output. Backpropagation, in its simplest form, measures statistics 
like this to make a model. Let's jump right in and use it to do this.

"""

import numpy as np

# sigmoid function,
# convert numbers to probabilities
def nonlin(x,deriv=False):
    if(deriv==True):
        return x*(1-x)
    return 1/(1+np.exp(-x))

if __name__ == "__main__":
    # input dataset
    X = np.array([  [0,0,1],
                    [0,1,1],
                    [1,0,1],
                    [1,1,1] ])
        
    # output dataset,
    # transposed into 4x1
    y = np.array([[0,0,1,1]]).T 
     
    # seed random numbers to make calculation
    # deterministic (just a good practice)
    np.random.seed(1)

    # The "neural network" is really just this matrix.
    # l0 and l1 are just transient values based on the dataset
    # All of the learning is stored in the syn0 matrix.
    syn0 = 2*np.random.random((3,1)) - 1 # initialize weights randomly with mean 0

    for iter in range(10000):

        # forward propagation
        l0 = X
        l1 = nonlin(np.dot(l0,syn0))

        # how much did we miss?
        l1_error = y - l1

        # multiply how much we missed by the 
        # slope of the sigmoid at the values in l1
        l1_delta = l1_error * nonlin(l1,True)

        # update weights
        syn0 += np.dot(l0.T,l1_delta)

    print("Output After Training:")
    print(l1)
