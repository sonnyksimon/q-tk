import math

def find(x, a): 
    """ return the lowest i such that a[i] == x if x is not in a. 
        (a is sorted in ascending order)"""
    return binary_search_in_range(x,a,0,len(a))

def binary_search_in_range(x, a, first, maxx): 
    """ return the lowest i 
        such that i <= i <= maxx and a[i] == x, 
        or -1 if there's no such i """
    if first >= maxx: return -1

    mid = (first+maxx)//2

    if x < a[mid]: 
        return binary_search_in_range(x, a, first, mid)
    elif x > a[mid]: 
        return binary_search_in_range(x, a, mid+1, maxx)
    else:
        if mid > 0 and x == a[mid-1]: 
            return binary_search_in_range(x, a, first, mid)
        else: 
            return mid

def linear_search(x, a): 
    for i in range(0, len(a)):
        if x == a[i]:
            return i
    return -1
