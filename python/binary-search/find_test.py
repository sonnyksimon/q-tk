#!/usr/bin/env python
# -*- coding: utf-8 -*-
"Unit tests for code in find.py"
import unittest
import find

class FindTest(unittest.TestCase):

    """ Testing strategy for i = search(x,a)
        ====================================
        partition the space of (x,a,i) as follows:
        
        x: neg, 0, pos
        a.length: 0, 1, 2+
        a.vals: neg, 0, pos. all same, increasing.
        i: 0, middle, n-1, -1 """

    def test_sanity(self):
        self.assertEqual(0,0)
    
    def test_empty(self):
        """ x=2, a=[-1, 1, 3], i=-1 """
        self.assertEqual(-1, find.find(1, []))

    def test_singleton(self):
        """ x=0, a=[0], i=0 """
        self.assertEqual(0, find.find(0, [0]))

    def test_middle(self):
        """ x=1, a=[-1, 1, 3], i=1 """
        self.assertEqual(1, find.find(1, [-1,1,3]))

    def test_start(self):
        """ x=-1, a=[-1, 1, 3], i=0 """
        self.assertEqual(0, find.find(-1, [-1,1,3]))

    def test_end(self):
        """ x=3, a=[-1, 1, 3], i=n-1 """
        self.assertEqual(2, find.find(3, [-1,1,3]))

    def test_not_found(self):
        """ x=2, a=[-1, 1, 3], i=-1 """
        self.assertEqual(-1, find.find(2, [-1,1,3]))

    def test_all_same(self):
        """ x=1, a=[1, 1, 1], i=0 """
        self.assertEqual(0, find.find(1, [1,1,1]))

if __name__ == '__main__':
    unittest.main()
