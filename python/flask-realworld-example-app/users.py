from flask import Blueprint, request, jsonify
from schema import User
from settings import secret_key
from auth import current_user
import bcrypt
import jwt
import datetime

users_page = Blueprint('users', __name__, template_folder='templates')

@users_page.route('/api/signup', methods=['POST'])
def signup():

    """

    POST **/api/signup**

    > Sign up for the site.
    >
    > `email` is used to login, there's no username.
    >
    > `password` is just a string.
    >
    > | name | description |
    > | - | - |
    > | message | Status of the request |
    
    """

    message = ""
    code = 500
    data = request.get_json()
    try:
        email = data['email'].lower()
        password = data['password']
        if email and password:
            if User.select().where(User.email == email).count() >= 1:
                message, code = "Mail exists", 409
            else:
                try:
                    hashed_pw = bcrypt.hashpw(password.encode('UTF-8'), bcrypt.gensalt())
                    user = User(email=email, password=hashed_pw)
                    user.save()
                    message, code ="User created", 201
                except:
                    message, code= "Error creating user", 500
    except:
        message, code ="No email or password given", 409
    return jsonify(dict(message=message)), code
     
@users_page.route('/api/login', methods=['POST'])
def login():

    """

    POST **/api/login**

    > Sign up for the site.
    >
    > `email` is used to login, there's no username.
    >
    > `password` is just a string.
    >
    > | name | description |
    > | - | - |
    > | token | A [json web token](http://jwt.io/). | 
    > | exp | Token expires at this timestamp. |
    > | message | Whether you failed, or not -.- |

    """

    message, code, token, exp = "", 500, "", ""
    data = request.get_json()
    try:
        email = data['email'].lower()
        password = data['password']
        if email and password:
            if User.select().where(User.email == email).count() < 1:
                message, code ="Auth failed", 401
            else:
                try:
                    hashed_pw = (User.select(User.password).where(User.email == email))[0].password
                    if bcrypt.checkpw( password.encode('UTF-8'), hashed_pw.encode('UTF-8') ):
                        expires = datetime.datetime.utcnow() + datetime.timedelta(seconds=60*30)
                        encoded = jwt.encode({'email': email, 'exp':expires}, secret_key, algorithm='HS256')
                        exp, token, message, code = expires, str(encoded), "Auth success", 200
                    else:
                        message, code = "Auth failed", 401
                except:
                    message, code = "Auth failed", 401
    except:
        message, code = "No email or password given", 409
    return jsonify(dict(token=token,exp=exp,message=message)), code

@users_page.route('/api/kick', methods=['POST'])
def kick():

    """

    POST **/api/kick** 

    > Must be logged in as `me@sonnyksimon.com` to kick users from my site.
    >
    > `email` is kicked from the site.
    >
    > | name | description |
    > | - | - |
    > | message | Status of the request |

    """

    email = request.get_json()['email']
    email = email.lower()
    message, code = "", 500
    if current_user() != 'me@sonnyksimon.com':
        message,code = "Only the user with email `me@sonnyksimon.com` can kick users from the site", 409
    elif User.select().where(User.email == email).count() < 1:
        message, code = "There is no user with that email.", 409
    else:
        q = User.delete().where(User.email == email)
        q.execute()
        deleted = (User.select().where(User.email == email).count() == 0)
        message = "%s was kicked from the site"%(email) if deleted else "Cannot kick this user for some reason"
    return jsonify(dict(message=message)), code
