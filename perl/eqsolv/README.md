`eqsolv` solves equations. 

It requires:

* the equation,

* a list of variables/symbols, 

* whether the solvable domain set should be R (reals), Z (integers), or C (complex) and

* an option to force results as numeric values
