#!/usr/bin/env python
import web

urls = (
    '/', 'index'
)

class index:
    def GET(self):
        return "index"

app = web.application(urls, globals())
wsgiapp = app.wsgifunc()
if __name__ == "__main__":
    app.run()
