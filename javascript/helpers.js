function simpleSum(a,b) {
  // Compute the sum of two integers.
  return a+b;
}

function simpleArraySum(ar) {
  // Given an array of integers, find the sum of its elements.
  return ar.reduce(function (total, elem) {
    return total + elem;
  }, 0);
}

function compareTriplets(a, b) {
  // return an array of two scores, comparing two triplets.
  var scores = [0,0];
  if (a.length !== b.length) return;
  for (var i = 0; i < a.length; i++) {
    if (a[i] > b[i]) scores[0]++;
    else if (b[i] > a[i]) scores[1]++;
  }
  return scores;
}

function diagonalDifference(arr) {
  // Given a square matrix, calculate the absolute difference 
  // between the sums of its diagonals.
  for (var diag1 = 0, diag2 = 0, i = 0; i < arr.length; i++)
        diag1 += arr[i][i], diag2 += arr[i][arr.length - (i + 1)];
    return Math.abs(diag1-diag2);
}
