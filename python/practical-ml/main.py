#!/usr/bin/env python3
import flask
import markdown

app = flask.Flask(__name__, static_url_path='/static', static_folder='public/static')

@app.route('/')
def index():
    return flask.render_template('index.html', md=markdown.markdown)

if __name__ == '__main__':
    app.run(debug=True)
