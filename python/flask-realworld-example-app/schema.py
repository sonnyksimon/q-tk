import peewee as sql
from settings import db
import re
email_re = re.compile(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")

class Table(sql.Model):
    class Meta:
        database = db

class Product(Table):
    name = sql.CharField()
    price = sql.DecimalField()
    productImage = sql.CharField()

class Order(Table):
    product = sql.ForeignKeyField(Product, backref='orders')
    quantity = sql.IntegerField(default=1)

class User(Table):
    email = sql.CharField(unique=True)
    password = sql.CharField()

def init():
    try:
        db.create_tables([Product,Order,User], safe=True)
    except Exception as e:
        raise e
