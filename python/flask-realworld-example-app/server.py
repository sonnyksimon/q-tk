#!/usr/bin/env python3
import os
from app import app

port = int(os.environ.get('PORT', '3000'))
os.environ['JSONIFY_PRETTYPRINT_REGULAR'] = str(False)
app.run(host='0.0.0.0', port=port, debug=True)
