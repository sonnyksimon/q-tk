`truthtbl.pl` automatically generates a truth table for any logical expre
ssion!

TODO:

task0: ensure you can compute a boolean expression in perl (AND, NOT, OR)

task1: Retrieve a list of alphabets in an expression (str)

task2: Retrieve a list of logic operators in an expression (str)

task3: generate 2^n possible combinations for n number of symbols, with 1 for True and 0 for False

task4: replace the variables and operators in the expression with a single combination and correct logical operators in perl. Ensure this string is a valid boolean expression.

task5: compute all combinations for the expression

task6: output the results as a markdown table

task7: map the markdown table to xml, json and csv
