#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Today: 2016.11.04

double f(double x) {
	double y = 0;
	y = sin(x);
	return y;
}

double integral(double a, double b, int N) {
	int i; double r=0; double w = (b-a)/N;
	for (i=1; i<=N; i++) r+= w*f(a-w/2+i*w);
	return r;
}

int main() {
	printf("Integral of f(x): %lf", integral(0,1,9999));
	return 0;
}
