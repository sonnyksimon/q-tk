_all_things = []

class metatracker(type):
    def __init__(self, name, bases, *a, **kw):
        type.__init__(self, name, bases, *a, **kw)
        if bases[0] != object and not hasattr(self, 'keys'):
            _all_things.append(self)
            self.keys = self._analyze(init=True)

class Thing(object):
    __metaclass__ = metatracker

    @classmethod
    def _name_(cls):
        return cls.__name__.lower()

    @classmethod
    def _analyze(cls, init=False):
        keys = dict()

        for k in dir(cls):
            if isinstance(getattr(cls,k), Datum):
                v = getattr(cls, k)
                v.name = v._name_(k)
                keys[k] = v
        return keys

    def __init__(self, row, ids=None):
        if ids: self._ids = ids

        c = self.__class__
        self.__class__ = type(c.__name__, c.__bases__, dict(c.__dict__))

        for k,v in self.keys.iteritems():
            if v.datatype:
                setattr(self, v.name, row[v.name])

class Datum(object): 
    def __init__(self, **kw):
        for k in kw:
            setattr(self, k, kw[k])

    _name_ = lambda self, k:k

    display = lambda self, x: unicode(x)

class String(Datum):
    datatype = 'str'
    def __init__(self, length=None, **kw):
        super(String, self).__init__(**kw)
        if length:
            self.datatype = 'str(%s)' % length

class Boolean(Datum):
    datatype = 'bool'
    display = lambda self, x: {True:'Yes', False: 'No', None: 'Unknown'}

class Integer(Datum): datatype = 'int'

class Float(Datum): datatype = 'float'

class Date(Datum): datatype = 'date'

class Year(Integer): pass

class Number(Integer): pass

class Dollars(Integer):
    display = lambda self, x: '$' + x

class Percentage(Float):
    precision = 3
    display = lambda self, x: str(x*100)[:self.precision +1] + '%'

class URL(String): pass
