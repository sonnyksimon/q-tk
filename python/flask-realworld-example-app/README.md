####TODO: Create a script that generates API docs from python code comments
####TODO: Write tests for testing the API

### api documentation
#### users

POST **/api/signup**

> Sign up for the site.
>
> `email` is used to login, there's no username.
>
> `password` is just a string.
>
> | name | description |
> | - | - |
> | message | Status of the request |

POST **/api/login**

> Sign up for the site.
>
> `email` is used to login, there's no username.
>
> `password` is just a string.
>
> | name | description |
> | - | - |
> | token | A [json web token](http://jwt.io/). | 
> | exp | Token expires at this timestamp. |
> | message | Whether you failed, or not -.- |

POST **/api/kick** 

> Must be logged in as `me@sonnyksimon.com` to kick users from my site.
>
> `email` is kicked from the site.
>
> | name | description |
> | - | - |
> | message | Status of the request |

#### products
#### orders
