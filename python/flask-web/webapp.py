#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import flask
import jwt
import datetime
from functools import wraps
from peewee import *
from playhouse.db_url import connect

class settings(object):
    production_mode = False
    db = connect('mysql://apps:apps@localhost:3306/flask_web')
    secret_key = 'thisisasecret'

app = flask.Flask(__name__,static_url_path='/static',static_folder='public/static')

def token_required(fn):
    @wraps(fn)
    def decorated_fn(*a, **kw):
        token = flask.request.args.get('token')

        if not token:
            return flask.jsonify(dict(message='Token is missing!')), 403

        try:
            data = jwt.decode(token, settings.secret_key)
        except:
            return flask.jsonify(dict(message='Token is invalid!')), 403

        return fn(*a, **kw)

    return decorated_fn

@app.route('/')
def hello():
    return flask.render_template('index.html')

@app.route('/api/protected')
@token_required
def protected():
    return flask.jsonify(dict(message="This is protected."))

@app.route('/api/login', methods=['POST'])
def login():

    if flask.request.is_json:
        data = flask.request.get_json()

        username = data.get('username')
        password = data.get('password')

        if username and password == 'password':
            token = jwt.encode({'user':username, 'exp':datetime.datetime.utcnow()+datetime.timedelta(minutes=30)}, settings.secret_key)
            return flask.jsonify(dict(token=token.decode('UTF-8')))
        
        return flask.make_response('Could not verify!', 403, {'WWW-Authenticate':'Basic realm:"Login Required"'})
    
    return flask.make_response('Bad data format!', 403, {'WWW-Authenticate':'Basic realm:"Login Required"'})

if __name__ == '__main__':
    app.run(debug=True)
