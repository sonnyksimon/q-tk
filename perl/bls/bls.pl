#!/usr/bin/perl

$str = "(a && b) || (a && c) || (b && !c)";

sub symbols
{
	$p_str = $_[0];
	$p_str =~ s/(.)(?=.*?\1)//g; # get only one occurance of a character
	my @symbols = $p_str =~ /[a-zA-Z]+/g; # extract alphabets
	return @symbols;
}
