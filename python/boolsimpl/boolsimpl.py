class Variable:
    name = None
    negation = None
    def __init__(self, name, negation):
        self.name = name
        self.negation = negation

class Node:
    operation = None
    negation = None
    variables = None
    nodes = None
    def __init__(self,operation,negation):
        self.operation = operation
        self.negation = negation
        self.variables = []
        self.nodes = []

class Expression:
    root = None
    def createAndGetRoot(self, operation):
        self.root = Node(operation, False)
        return self.root

class ExpressionParser:
    def parse(exp):
        res = Expression()
        node = Node("EMPTY", False)

        var1 = None
        if (exp[1:1] == "^"):
            var1 = Variable(exp[2:], True)
        else:
            var1 = Variable(exp, False)

        node.variables = list(var1)
        res.root = node
        return res

class Evaluator:
    def simplifyExpression(e):
        return None

    def buildDataStructureOfExpression(e):
        tree = Expression()
        return tree
