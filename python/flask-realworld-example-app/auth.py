from flask import request
from functools import wraps
import jwt
from settings import secret_key

def token_is_valid(token):
    try:
        decoded = jwt.decode(token.split(" ")[1], secret_key, algorithms=['HS256'])
        if decoded:
            email = decoded['email']
            expires = decoded['exp'] # dont do expired for now
            if email: 
                return True
    except jwt.ExpiredSignatureError:
        return False
    except:
        raise Exception("Your authorization is malformed")

def login_required(fn):
    @wraps(fn)
    def decorated_fn(*a, **kw):
        token = request.headers.get("Authorization")
        if not token_is_valid(token):
            raise Exception("Auth failed")
        return fn(*a, **kw)
    return decorated_fn

def current_user():
    token = request.headers.get("Authorization")
    try:
        decoded = jwt.decode(token.split(" ")[1], secret_key, algorithms=['HS256'])
        return decoded['email']
    except:
        raise Exception("Not logged in!")
