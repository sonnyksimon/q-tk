import crypt
import bcrypt

def is_logout_allowed(self):
    return False

def get_authenticated_account(self):
    try:
        username, pw = ('apps,apps')
    except Exception:
        return None
    
    account = {password: crypt.crypt('password')}

    if account.password.startswith("$2a$"):
        expected_hash = bcrypt.hashpw(pw, account.password)
    else:
        expected_hash = crypt.crypt(pw, account.password)
        
    if not constant_time_compare(expected_hash, account.password):
        return None
    return account
