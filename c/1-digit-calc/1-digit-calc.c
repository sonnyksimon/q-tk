#include <stdio.h>
#include <limits.h>

// Today: 2016.07.06

char input;
int firstnumber=(INT_MAX);
int secondnumber=(INT_MAX);
int answer=99;
int result;
int Check=(INT_MAX-1);
int expression=1;

void Calculate(char input);

int main() {
	printf(">> ");
	
	while(expression != 0) {
	
	scanf("%c", &input);
	
	Calculate(input);
	}
	
	if(answer==1) {
		result=firstnumber+secondnumber;
	} else if(answer==2) {
		result=firstnumber-secondnumber;
	} else if(answer==3) {
		result=firstnumber*secondnumber;
	} else if(answer==4) {
		result=firstnumber/secondnumber;
	}
	printf("\n>> %i", result);
	
	return 0;
}

void Calculate(char input) {
		switch(input) {
		case '1':
			if(Check==(INT_MAX-1)){
				firstnumber=1;
				Check=INT_MAX;
			} else {
				secondnumber=1;
				expression=0;
			}
			break;
		case '2':
			if(Check==(INT_MAX-1)){
				firstnumber=2;
				Check=(INT_MAX);
			} else {
				secondnumber=2;
				expression=0;
			}
			break;
		case '3':
			if(Check==(INT_MAX-1)){
				firstnumber=3;
				Check=(INT_MAX);
			} else {
				secondnumber=3;
				expression=0;
			}
			break;
		case '4':
			if(Check==(INT_MAX-1)){
				firstnumber=4;
				Check=(INT_MAX);
			} else {
				secondnumber=4;
				expression=0;
			}
			break;
		case '5':
			if(Check==(INT_MAX-1)){
				firstnumber=5;
				Check=(INT_MAX);
			} else {
				secondnumber=5;
				expression=0;
			}
			break;
		case '6':
			if(Check==(INT_MAX-1)){
				firstnumber=6;
				Check=(INT_MAX);
			} else {
				secondnumber=6;
				expression=0;
			}
			break;
		case '7':
			if(Check==(INT_MAX-1)){
				firstnumber=7;
				Check=(INT_MAX);
			} else {
				secondnumber=7;
				expression=0;
			}
			break;
		case '8':
			if(Check==(INT_MAX-1)){
				firstnumber=8;
				Check=(INT_MAX);
			} else {
				secondnumber=8;
				expression=0;
			}
			break;
		case '9':
			if(Check==(INT_MAX-1)){
				firstnumber=9;
				Check=(INT_MAX);
			} else {
				secondnumber=9;
				expression=0;
			}
			break;
		case '+':
			if(firstnumber==(INT_MAX)){
				printf("\n>> Error!");
				expression=0;
			} else {
				answer=1;
			}
			break;
		case '-':
			if(firstnumber==(INT_MAX)){
				printf("\n>> Error!");
				expression=0;
			} else {
				answer=2;
			}
			break;
		case '*':
			if(firstnumber==(INT_MAX)){
				printf("\n>> Error!");
				expression=0;
			} else {
				answer=3;
			}
			break;
		case '/':
			if((firstnumber==(INT_MAX))||(secondnumber==0)){
				printf("\n>> Error!");
				expression=0;
			} else {
				answer=4;
			}
			break;
		default:
			puts("Error!");
			expression=0;
			break;
	}
}
