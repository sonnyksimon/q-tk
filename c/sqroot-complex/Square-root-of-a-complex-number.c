#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

// Today: 2016.05.18

/*The me from the future, I forgot to do the y with the square root and instead used y=b/2x; it is not wrong but can be made better :p*/

void squareroot();

int main()
{
    char *choice = malloc(sizeof(char));
    int terminate=0;
    printf("1. Calculate the square root of a number\n0.Exit\nChoice: ");
    scanf("%s", choice);
    if(strlen(choice)==1)
    {
        switch(choice[0])
        {
        case '0':
            terminate=1;
            break;
        case '1':
            squareroot();
            break;
        }
    }
    free(choice);
    if(terminate!=1)
    {
    	getch();
    	printf("\n");
        main();
    }
    return 0;
}

void squareroot()
{
    printf("\nComplex Number (a+bi)\nNote! You might have to press enter twice.\nEnter a: ");
    float a;
    scanf("%f", &a);
    getch();
    printf("Enter b: ");
    float b;
    scanf("%f", &b);
    getch();

    printf("\nAnswer in form (x = c1, y = c2) where number = ( x + iy )\na=%.3f b=%.3f", a, b);

    float x1 = sqrt((0.5*(a+sqrt((a*a)+(b*b)))));
    if(x1!=0)
    {
        float y1 = b/(2*x1);
        printf("\n1. x = %.3f, y=%.3f", x1, y1);
    }
    else
    {
        printf("\n1. x = 0.000, y includes division by zero");
    }

    if((sqrt((a*a)+(b*b)))>a)
    {
        float x2 = sqrt((0.5*(sqrt((a*a)+(b*b))-a)));
        if(x2!=0)
        {
            float y2 = b/(2*x2);
            printf("\n2. x = %.3f, y=%.3f", y2, x2);
        }
        else
        {
            printf("\n2. x = 0.000, y includes division by zero");
        }
    }
    else
    {
        float x2 = sqrt((0.5*(a-sqrt((a*a)+(b*b)))));

        if(x2!=0)
        {
            float y2 = b/(2*x2);
            printf("\n2. x = %.3f, y=%.3f", x2, y2);
        }
        else
        {
            printf("\n2. x = 0.000, y includes division by zero");
        }
    }

    float x3 = -1*sqrt(((0.5*(a+sqrt((a*a)+(b*b))))));
    if(x3!=0)
    {
        float y3 = b/(2*x3);
        printf("\n3. x = %.3f, y=%.3f", x3, y3);
    }
    else
    {
        printf("\n3. x = 0.000, y includes division by zero");
    }

    if((sqrt((a*a)+(b*b)))>a)
    {
        float x4 = -1*sqrt((0.5*(sqrt((a*a)+(b*b))-a)));
        if(x4!=0)
        {
            float y4 = b/(2*x4);
            printf("\n4. x = %.3f, y = %.3f", y4, x4);
        }
        else
        {
            printf("\n4. x = 0.000, y includes division by zero");
        }
    }
    else
    {
        float x4 = -1*sqrt((0.5*(a-sqrt((a*a)+(b*b)))));
        if(x4!=0)
        {
            float y4 = b/(2*x4);
            printf("\n4. x = %.3f, y=%.3f", x4, y4);
        }
        else
        {
            printf("\n4. x = 0.000, y includes division by zero");
        }
    }
    printf("\n");
}

