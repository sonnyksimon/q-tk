from flask import Flask, request, jsonify, make_response
from functools import wraps

app = Flask(__name__)
app.url_map.strict_slashes = False
app.config['TRAP_HTTP_EXCEPTIONS'] = True

from products import products_page
from orders import orders_page
from users import users_page

@app.errorhandler(Exception)
def every_error(err):
    try:
        code = err.code
    except:
        code = 500
    finally:
        return jsonify(dict(error=dict(message=str(err)))), code

app.register_blueprint(products_page)
app.register_blueprint(orders_page)
app.register_blueprint(users_page)
