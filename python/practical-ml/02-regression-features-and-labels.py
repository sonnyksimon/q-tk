#!/usr/bin/env python3
import pandas as pd
import quandl
import math

df = quandl.get('WIKI/GOOGL')

df = df[['Adj. Open', 'Adj. High', 'Adj. Low', 'Adj. Close', 'Adj. Volume' ]]
df['HL_PCT'] = (df['Adj. High'] - df['Adj. Close']) / df['Adj. Close'] * 100.0
df['PCT_change'] = (df['Adj. Close'] - df['Adj. Open']) / df['Adj. Open'] * 100.0

# features
df = df[['Adj. Close', 'HL_PCT','PCT_change','Adj. Volume']]

forecast_col = 'Adj. Close'
df.fillna(-99999, inplace=True) #mark NaNs as outliers

# trying to predict 1% of the dataframe
forecast_out = int(math.ceil( 0.01 * len(df) ))

df['label'] = df[forecast_col].shift(-forecast_out) # adjust label column for Adj. Close (1 percent into the future)
df.dropna(inplace=True)
print(df.head())
