from flask import Blueprint, request, jsonify

orders_page = Blueprint('orders', __name__, template_folder='templates')

@orders_page.route('/api/orders', methods=['GET','POST'])
def orderss():
    if request.method == 'GET':
        return jsonify(dict(message='Orders were fetched')), 200
    elif request.method == 'POST':
        try:
            data = request.get_json()
            order = dict(productId=data['productId'], quantity=data['quantity'])
            return jsonify(dict(message='Order was created', order=order)), 201
        except:
            raise Exception("Invalid input")

@orders_page.route('/api/orders/<id>', methods=['GET','DELETE'])
def order(id):
    if request.method == 'GET':
        return jsonify(dict(message='Order details', orderId=id)), 200
    elif request.method == 'DELETE':
        return jsonify(dict(message='Order deleted')), 200
